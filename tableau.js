
// Exercice 30

console.log("Exercice 30");
var tempChar = "A";
var tab4 = ["D", "E", "C", "A", "L", "A", "G", "E"];

console.log(tab4);
for (let u = 0; u < tab4.length - 1; u++) {
    tempChar = tab4[u + 1];
    tab4[u + 1] = tab4[u];
    tab4[u] = tempChar;

}

console.log(tab4);


// Exercice 31

console.log("Exercice 31");
var isTrie = true;
let tab5 = [1, 4, 7, 7, 18, 8, 9, 10];

for (let index = 0; index < tab5.length; index++) {

    if (tab5[index] > tab5[index + 1]) {
        isTrie = false;
        break;

    }
}
if (isTrie) {

    console.log("Le tableau est trié de manière croissante");

}
else {
    console.log("Le tableau n'est pas trié de manière croissante");
}





// Exercice 32

console.log("Exercice 32");

let tab2 = [23, 78, 90, 3, 2, 6, 4, 67];

let tempo = 0;

i = 0;
j = tab2.length - 1;

while (i < j) {

    tempo = tab2[i];
    tab2[i] = tab2[j];
    tab2[j] = tempo;

    i++;
    j--;

}


console.log(tab2);

//Exercice 33

console.log("Exercice 33");

let indiceMin = 0;
let indice = 0;
let temp = 0;

let tab = [2, 6, 9, 1, 56, 45, 90, 12];
while (indice < tab.length - 1) {
    indiceMin = indice + 1;
    for (let index = indice; index < tab.length - 1; index++) {
        if (tab[indiceMin] > tab[index]) {
            indiceMin = index
        }
    }
    temp = tab[indiceMin];
    tab[indiceMin] = tab[indice]
    tab[indice] = temp;
    indice++;
}
console.log(tab)

